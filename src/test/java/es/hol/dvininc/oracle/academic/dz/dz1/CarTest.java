package es.hol.dvininc.oracle.academic.dz.dz1;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CarTest {

    @Test
    public void test1(){
        Car car1 = new Car(){
            public String toString(){
                return "CAR1111111111111111111111111111111111111111\n";
            }
            public boolean equals(Object o) {
                return true;
            }
        };
        Car car2 = new Car(){
            public String toString(){
                return "CAR22222222222222222222222222222222222222222222\n";
            }
        };
        System.out.print(car1.toString());
        System.out.print(car2.toString());
        assertTrue(car1.equals(car2));
        assertFalse(car2.equals(car1));
    }
}