package es.hol.dvininc.oracle.academic.dz.dz2;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class StringUtilsTest {
    private StringUtils stringUtils;

    @Before
    public void start() {
        stringUtils = new StringUtils();
    }

    @Test
    public void palindrome() throws Exception {
        assertTrue(stringUtils.palindrome("А роза упала на лапу Азора"));
        assertFalse(stringUtils.palindrome("2А роза упала на лапу Азора"));
    }

    @Test
    public void check_length() throws Exception {
        assertEquals(stringUtils.check_length("Hello world!"), "Hello ");
        assertEquals(stringUtils.check_length("Hello"), "Helloooooooo");
    }

    @Test
    public void turn_string() throws Exception {
        assertEquals(stringUtils.turn_string("Hello world!"), "!dlrow olleH");
    }

    @Test
    public void change_words() throws Exception {
        assertEquals(stringUtils.change_words("Hello world!"), "world Hello!");
    }

    @Test
    public void change_words_in_sentences() throws Exception {
        assertEquals(stringUtils.change_words_in_sentences("Регулярное выражение это своего рода шаблон, который может быть применен к тексту (String, в Java). Java предоставляет пакет java.util.regex для сопоставления с регулярными выражениями."), "Java выражение это своего рода шаблон, который может быть применен к тексту (String, в Регулярное). java предоставляет пакет Java.util.выражениями для сопоставления с регулярными regex.");
    }

    @Test
    public void hasABC() throws Exception {
        assertTrue(stringUtils.hasABC("abc"));
        assertFalse(stringUtils.hasABC("abcd"));
    }

    @Test
    public void hasDate() throws Exception {
        assertTrue(stringUtils.hasDate("12.31.15"));
        assertFalse(stringUtils.hasDate("12.31.151"));
    }

    @Test
    public void hasPost() throws Exception {
        assertTrue(stringUtils.hasPost("61000"));
        assertFalse(stringUtils.hasPost("845654sa"));
    }

    @Test
    public void arrayOfTel() throws Exception {
        assertEquals(stringUtils.arrayOfTel("функция, которая проверяет содержит ли строка только символы 'a', 'b', 'c' или нет.\n" +
                "функция, которая определят является ли строчка датой формата MM.DD.YYYY\n" +
                "функция, которая определяет является ли строчка почтовым адресом\n" +
                "функция, которая находит все телефоны в переданном тексте формата +3(888)888-88-88, и возвращает их в виде массива").toString(),"[+3(888)888-88-88]");
    }
}