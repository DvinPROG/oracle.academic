package es.hol.dvininc.oracle.academic.pr.pr1;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ArraySumTest {
    static ArraySum arraySum1;

    @BeforeClass
    public static void before_start() {
        arraySum1 = new ArraySum(new int[]{1, 2, 3, 4, 5});
    }

    @Test
    public void testSum1() throws Exception {
        ArraySum arraySum = new ArraySum();
        assertEquals(arraySum.sum(new int[]{1, 2, 3, 4, 5}), 15);
    }

    @Test
    public void testSum2() throws Exception {
        assertEquals(arraySum1.sum(), 15);
    }

    @Test (expected = NullPointerException.class)
    public void testSum3()throws Exception{
        arraySum1.sum(null);
    }
}