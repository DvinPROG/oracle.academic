package es.hol.dvininc.oracle.academic.pr.pr1.tdd;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class StudentTest {
    static Student student;

    @Before
    public void start(){
        ArrayList<Exam> exams = new ArrayList<Exam>();
        exams.add(new Exam("Math", 92,2016,1));
        exams.add(new Exam("Diskretka",96, 2016,1));
        exams.add(new Exam("IYKKC",96, 2016,1));
        exams.add(new Exam("Fizika",-1, 2016,1));
        student = new Student("Sasha", "Kerich", new Group("KIYKI-16-4", "KIY"), exams);
    }


    @Test
    public void testMax_mark() throws Exception {
        assertEquals(student.max_mark(), 96);
    }


    @Test
    public void testAdd_mark() throws Exception {
        student.add_mark("Math", 20);
        assertEquals(student.exams.get(0).mark, 20);
    }

    @Test
    public void testDelete_mark() throws Exception {
        student.delete_mark("Math");
        assertEquals(student.exams.get(0).mark, -1);
    }

    @Test(expected = Exception.class)
    public void testIs_get_exam() throws Exception {
        student.is_get_exam("gn");
    }

    @Test
    public void testAmount_of_exam_with_mark() throws Exception {
        assertEquals(student.amount_of_exam_with_mark(96), 2);
    }

    @Test
    public void testMiddle_mark_of_one_semestr() throws Exception {
        ArrayList<Exam> exams = new ArrayList<Exam>();
        assertEquals(student.middle_mark_of_one_semestr(2016, 1), 94);
    }
}