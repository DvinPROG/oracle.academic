package es.hol.dvininc.oracle.academic.dz.dz1;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DateTest {
    private static Date date;

    @BeforeClass
    public static void start(){
        date = new Date (27,1,2017);
    }

    @Test
    public void createClassDate() throws Exception {
        assertEquals(date.getDayOfWeek(), Date.DayOfWeek.FRIDAY);
    }

    @Test
    public void getDayOfYear() throws Exception {
        assertEquals(date.getDayOfYear(),27);
    }

    @Test
    public void getdaysBetween() throws Exception {
        assertEquals(date.daysBetween(new Date(29, 1, 2017)),2);

    }
}