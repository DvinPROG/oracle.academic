package es.hol.dvininc.oracle.academic.pr.pr4;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Давид on 14.02.2017.
 */
public class StudentUtilsTest {
    @Test
    public void amountOfWords1() throws Exception {
        System.out.println(StudentUtils.amountOfWords("Romeo.txt", StudentUtils.Sort.AMOUNT_OF_WORDS, StudentUtils.Direction.UP).toString());
    }

    @Test
    public void amountOfWords() throws Exception {
        //System.out.println(StudentUtils.amountOfWords("Romeo.txt").toString());
    }

    static List<Student> students;

    @Before
    public void beforeStart(){
        students = new ArrayList<>(Arrays.asList(
                new Student("David", "Dvinskih", 1),
                new Student("Anastasia", "Pavlushenko", 1),
                new Student("Some", "Man", 1),
                new Student("Every", "Student", 5)
        ));
    }

    @Test
    public void createMapFromList() throws Exception {

    }

    @Test
    public void printStudents() throws Exception {

    }

    @Test
    public void sortStudent() throws Exception {
        System.out.println(StudentUtils.sortStudent(students).toString());
    }



}