package es.hol.dvininc.oracle.academic.pr.pr1;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ArrayProdTest {
    static ArrayProd arrayProd;

    @BeforeClass
    public static void start(){
        arrayProd = new ArrayProd(new int[]{1, 2, 3, 4, 5});
    }

    @Test
    public void testProd() throws Exception {
        assertEquals(arrayProd.prod(), 120);
    }
}