package es.hol.dvininc.oracle.academic.pr.pr6;

import es.hol.dvininc.oracle.academic.pr.pr5.FileUtils;

import java.io.File;

/**
 * Created by Давид on 28.02.2017.
 */
public class Task4 {
    public static void main(String[] args) {
        File startPath = new File("D:\\Свои программы");
        File endPath = new File("D:\\Свои программы1");
        endPath.mkdirs();
        if (startPath.isDirectory())
            for (File file : startPath.listFiles()) {
                new Thread(() -> FileUtils.copyFile(file.getAbsolutePath(), endPath+"\\"+file.getName())).start();
            }
    }
}
