package es.hol.dvininc.oracle.academic.dz.dz4.task4_2.part3;

public interface ListIterable<E> {
    ListIterator<E> listIterator();
}
