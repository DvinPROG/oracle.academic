package es.hol.dvininc.oracle.academic.pr.pr1.tdd;

import java.util.ArrayList;

public class Student {
    String name;
    String surname;
    Group group;
    public ArrayList<Exam> exams;

    public Student(String name, String surname, Group group, ArrayList<Exam> exams) {
        this.name = name;
        this.surname = surname;
        this.group = group;
        this.exams = exams;
    }

    public int max_mark() {
        int max = 0;
        for (Exam exam : exams) {
            if (max < exam.getMark()) max = exam.getMark();
        }
        return max;
    }

    public void add_mark(String name_exam, int mark) throws Exception {
        is_get_exam(name_exam);
        Exam cur_exam = null;
        for (Exam exam : exams) {
            if (exam.getName() == name_exam) {
                cur_exam = exam;
                break;
            }
        }
        cur_exam.setMark(mark);
    }

    public void delete_mark(String name_exam) throws Exception {
        is_get_exam(name_exam);
        Exam cur_exam = null;
        for (Exam exam : exams) {
            if (exam.getName() == name_exam) {
                cur_exam = exam;
                break;
            }
        }
        cur_exam.setMark(-1);
    }

    public boolean is_get_exam(String name_exam) throws Exception {
        Exam cur_exam = null;
        for (Exam exam : exams) {
            if (exam.getName() == name_exam) {
                cur_exam = exam;
                break;
            }
        }
        if (cur_exam == null) throw new Exception("Такой экзамен не сдавали");
        return cur_exam.getMark() != -1;
    }

    public int amount_of_exam_with_mark(int mark) {
        int result = 0;
        for (Exam exam : exams) {
            if (exam.getMark() == mark) {
                result++;
            }
        }
        return result;
    }

    public int middle_mark_of_one_semestr(int year, int semestr) {
        int result = 0;
        int amount = 0;
        for (Exam exam : exams) {
            if ((exam.getMark() != -1) && (exam.getDate().getYear() == year) && (exam.getDate().getSemestr() == semestr)) {
                result += exam.getMark();
                amount++;
            }
        }
        return (amount != 0) ? result / amount : 0;
    }
}
