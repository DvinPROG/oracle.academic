package es.hol.dvininc.oracle.academic.pr.pr6;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Task3 {
    static FileWriter fw;

    static {
        try {
            fw = new FileWriter(new File("allTxt"));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static void main(String[] args) throws IOException {
        folder(new File("D:\\"));
    }

    public static void folder(File f) {
        new Thread(() -> {
            try {
                if (f.listFiles() != null)
                    for (File file : f.listFiles()) {
                        if (file.isDirectory())
                            folder(file);
                        if (file.getName().contains(".exe")) {
                            mime(file);
                        }
                    }
            } catch (OutOfMemoryError e){
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                e.printStackTrace();
            }
        }).start();
    }


    private static void mime(File file) {
        System.out.println(Thread.currentThread().getName() + " \t" + file.getAbsolutePath());
        try {
            fw.write(Thread.currentThread().getName() + " \t" + file.getAbsolutePath() + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}