package es.hol.dvininc.oracle.academic.pr.pr3;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;

/**
 * Created by David on 13.02.2017.
 */
@Getter
@Setter
public class User {
    private String name;
    private String surname;
    private String login;
    private String pass;
    ArrayList<Ticket> tickets;
    {
        tickets = new ArrayList<Ticket>();
    }

}
