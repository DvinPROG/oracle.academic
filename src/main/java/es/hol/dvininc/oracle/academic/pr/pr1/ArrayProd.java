package es.hol.dvininc.oracle.academic.pr.pr1;

public class ArrayProd {
    public int[] getElm() {
        return elm;
    }

    public void setElm(int[] elm) {
        this.elm = elm;
    }

    private int elm[];
    public ArrayProd(int array[]){
        elm = array;
    }
    public int prod(){
        int prod = 1;
        for (int element:getElm()){
            prod *= element;
        }
        return prod;
    }
}
