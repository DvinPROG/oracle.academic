package es.hol.dvininc.oracle.academic.pr.pr1;

public class ArraySum {
    int elm[];

    public ArraySum(int array[]) {
        setElm(array);
    }

    public ArraySum() {

    }

    public static int sum(int[] elements) {
        int sum = 0;
        for (int element : elements) {
            sum += element;
        }
        return sum;
    }

    public int[] getElm() {
        return elm;
    }

    public void setElm(int[] elm) {
        this.elm = elm;
    }

    public int sum() {
        int sum = 0;
        for (int element : getElm()) {
            sum += element;
        }
        return sum;
    }
}

