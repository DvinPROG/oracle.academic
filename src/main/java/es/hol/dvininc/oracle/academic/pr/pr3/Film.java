package es.hol.dvininc.oracle.academic.pr.pr3;

import java.util.ArrayList;

/**
 * Created by Давид on 07.02.2017.
 */
public class Film {
    String name;
    String description;
    ArrayList<Session> sessions  = new ArrayList<Session>();

    public Film(String name, String description, ArrayList<Session> sessions) {
        this.name = name;
        this.description = description;
        this.sessions = sessions;
    }
}
