package es.hol.dvininc.oracle.academic.pr.pr1.tdd;

import lombok.Data;

@Data
public class Exam {
    public int mark;
    Date date;
    String name;

    public Exam(String name, int mark, int year, int semestr) {
        this.name = name;
        this.mark = mark;
        date = new Date(year, semestr);
    }

    public Exam(String name, int year, int semestr) {
        this.name = name;
        mark = -1;
        date = new Date(year, semestr);
    }

    @Data
    public class Date {
        private int year;
        private int semestr;

        public Date(int year, int semestr) {
            this.year = year;
            this.semestr = semestr;
        }
    }
}
