package es.hol.dvininc.oracle.academic.pr.pr6;

/**
 * Created by Давид on 28.02.2017.
 */
public class Task1 {
    static class ClassThread extends Thread{
        @Override
        public void run() {
            while(true) {
                System.out.println(Thread.currentThread().getName());
                try {
                    sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
            }
        }
    }

    public static void main(String[] args) {
        ClassThread classThread = new ClassThread();
        Thread thread =new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    System.out.println(Thread.currentThread().getName());
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        return;
                    }
                }
            }
        });
        classThread.start();
        thread.start();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        classThread.interrupt();
        thread.interrupt();
    }
}
