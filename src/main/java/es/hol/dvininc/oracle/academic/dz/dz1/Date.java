package es.hol.dvininc.oracle.academic.dz.dz1;

import lombok.Data;
import lombok.Getter;

import static java.lang.Math.abs;

@Data
public class Date {
    private int day;
    private Month month;
    private Year year;
    private DayOfWeek dayOfWeek;

    public Date(int day, int month, int year) {
        this.day = day;
        this.month = new Month(month);
        this.year = new Year(year);
        int a = (14 - month) / 12, y = year - a, m = month + 12 * a - 2;
        dayOfWeek = DayOfWeek.valueOf((7000 + (day + y + y / 4 - y / 100 + y / 400 + (31 * m) / 12) - 1) % 7);
        System.out.print(getDayOfWeek().toString());
    }

    public DayOfWeek getDayOfWeek() {
        return dayOfWeek;
    }

    public int getDayOfYear() {
        int result = 0;
        Month month_zero = new Month(1);
        result += month_zero.getDays(month.getMonth(), year.isLeapYear()) + getDay();
        return result;
    }

    public int daysBetween(Date date) {
        //java.util.Date diff =  new java.util.Date(new java.util.Date(getYear().getYear(),getMonth().getMonth(),getDay()).getTime() - new java.util.Date(date.getYear().getYear(),date.getMonth().getMonth(),date.getDay()).getTime());
        if (getYear().getYear() == date.getYear().getYear()) return abs(getDayOfYear() - date.getDayOfYear());
        else {
            int result = 0;
            Date min_date;
            Date max_date;
            if (getYear().getYear() > date.getYear().getYear()) {
                min_date = date;
                max_date = this;
            } else {
                min_date = this;
                max_date = date;
            }
            for (int i = min_date.getYear().getYear(); i <= max_date.getYear().getYear(); i++) {
                if (i == min_date.getYear().getYear()) {
                    result = 365 - min_date.getDayOfYear();
                    if (min_date.getYear().isLeapYear()) result++;
                } else if (i == max_date.getYear().getYear()) result += max_date.getDayOfYear();
                else {
                    result += 365;
                    Year year = new Year(i);
                    if (year.isLeapYear()) result++;
                }
            }
            return result;
        }
    }

    enum DayOfWeek {
        MONDAY(0), TUESDAY(1), WEDNESDAY(2), THURSDAY(3), FRIDAY(4), SATURDAY(5), SUNDAY(6);
        private int index;

        DayOfWeek(int index) {
            this.index = index;
        }

        public static DayOfWeek valueOf(int index) {
            switch (index) {
                case 0:
                    return DayOfWeek.MONDAY;
                case 1:
                    return DayOfWeek.TUESDAY;
                case 2:
                    return DayOfWeek.WEDNESDAY;
                case 3:
                    return DayOfWeek.THURSDAY;
                case 4:
                    return DayOfWeek.FRIDAY;
                case 5:
                    return DayOfWeek.SATURDAY;
                case 6:
                    return DayOfWeek.SUNDAY;
            }
            return null;
        }
    }

    @Getter
    public class Year {
        private boolean leapYear;
        private int year;

        public Year(int year) {
            this.year = year;
            leapYear = year % 4 == 0;
        }

    }

    @Getter
    public class Month {
        private int[] day_in_month = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        private int month;

        public Month(int month) {
            this.month = month;
        }

        public int getDays(int monthNumber, boolean leapYear) {
            int result = 0;
            if (month < monthNumber) {
                for (int i = month - 1; i < monthNumber - 1; i++) {
                    result += day_in_month[i];
                    if ((i == 1) && (leapYear)) result++;
                }
            } else if (month > monthNumber) {
                for (int i = month - 1; i < 12; i++) {
                    result += day_in_month[i];
                    if ((i == 1) && (leapYear)) result++;
                }
                for (int i = 0; i < monthNumber - 1; i++) {
                    result += day_in_month[i];
                    if ((i == 1) && (leapYear)) result++;
                }
            }
            return result;
        }

    }
}
