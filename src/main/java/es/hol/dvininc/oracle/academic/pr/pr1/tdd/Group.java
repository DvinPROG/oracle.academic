package es.hol.dvininc.oracle.academic.pr.pr1.tdd;

/**
 * Created by student on 1/25/2017.
 */
public class Group {
    String course;
    String faculty;

    public Group(String course, String faculty) {
        this.course = course;
        this.faculty = faculty;
    }
}
