package es.hol.dvininc.oracle.academic.dz.dz1;

public class Car {
    //Объекты этих классов я создаю в тестах.

    private String name = "default";

    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        return name.equals(car.name);
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                '}';
    }
}
